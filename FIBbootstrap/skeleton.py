# Copyright 2016 Joshua Taillon
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import glob
import numpy as _np
from .utils import calculate_errors
import pandas as _pd
from datetime import datetime as _dt
from tqdm import tqdm

__all__ = ['bootstrap_skel_stats',
           'find_nodes',
           'analyze_avizo_skel_csv',
           'calc_skel_params']


# noinspection PyTypeChecker
def find_nodes(input_fname,
               node_output='nodes.txt',
               save_output=True,
               return_type='str',
               debug=False):
    """
    Calculate and write a data file describing the connectivity of nodes
    within a network (saved in an .mv3d file from Avizo)

    Parameters
    ----------
    input_fname: str
        .mv3d filename to read
    node_output: str
        name of text file to write to, if saving the output
    save_output: bool
        switch to control whether a file is written to disk (output format
        that matches the old
        :download:`findNodes.sh <../old_bash/findNodes.sh>` script)
    return_type: str
        passed to :py:meth:`numpy.ndarray.astype` to determine how the output
        should be formatted upon return. Default is as a string,
        so no information contained in the original .mv3d file is modified,
        but oftentimes ``'float32'`` would be more useful for calculating
        statistics
    debug: bool
        switch to control debugging output

    Returns
    -------
    data: :class:`~numpy.ndarray`
        Contains the output data in a numpy array with columns of:

        +-----------+----------+----------+----------+---------+
        |:math:`k_i`|x position|y position|z position|thickness|
        +-----------+----------+----------+----------+---------+

    num_edges:  :class:`float`
        Number of edges (E) in skeleton
    num_nodes: :class:`float`
        Number of nodes (N) in skeleton
    mean_k: :class:`float`
        Average node connectivity (<k>)
    """

    if debug:
        print('1/14: Reading data from', input_fname)

    with open(input_fname, "r") as i_file:
        lines = i_file.readlines()[7:]  # read data into list, removing header

    # Get the directory name:
    dir_name = os.path.abspath(os.path.join(os.path.abspath(input_fname),
                                            '..'))

    if debug:
        print('2/14: Removing empty lines from file')
    # remove empty lines:
    # while '\n' in lines:
    #     lines.remove('\n')
    lines = [x for x in lines if x != '\n']

    if debug:
        print('3/14: Trimming tabs and newlines')
    # replace tabs with spaces and trim newline characters
    lines = [x.replace('\n', '').split('\t') for x in lines]
    # for i, l in enumerate(lines):
    #     lines[i] = l.replace('\n', '').split('\t')

    if debug:
        print('4/14: Converting data to numpy')
    # Convert to a numpy array, rather than just a list
    data = _np.array(lines)

    if len(data) == 0:
        num_edges = 0
        num_nodes = 0
        mean_k = _np.nan

    else:
        if debug:
            print('5/14: Finding number of edges')
        # Find unique lines in first column (number of edges)
        num_edges = len(set(data[:, 0]))

        # Get first column, which is the index (number) of each edge
        edgenums = data[:, 0]

        if debug:
            print('6/14: Finding where each edge ends')
        edge_end_locs = _np.where(_np.diff(edgenums.astype('int')) == 1)[0]

        # Determine where the end points of each edge are:
        end_idx = _np.zeros(1 + 2 * len(edge_end_locs) + 1, dtype='int')

        end_idx[0] = 0  # always one at zero

        if debug:
            print(edge_end_locs)
            print('Length of end_idx', len(end_idx))
            print('Length of edge_end_locs', len(edge_end_locs))

        if debug:
            print('7/14: Adding position to end_idx')
        ii = 1
        for i in edge_end_locs:
            # find where each edge ends, and add position and next start to
            # list
            end_idx[ii] = i
            end_idx[ii + 1] = i + 1
            ii += 2

        # add the last point, too
        end_idx[-1] = len(edgenums) - 1

        if debug:
            print('Length of end_idx', len(end_idx))

        # end_points = data[end_idx] is now a list of edge end points
        end_points = data[end_idx]

        if debug:
            print('8/14: Joining end points into strings')
        # join each row of the end points list into a single string
        string_list = [' '.join(row) for row in end_points[:, 1:]]

        if debug:
            print('9/14: Count how many times each end point appears')
        # count how many times each end point shows up
        edges, counts = _np.unique(string_list, return_counts=True)

        if debug:
            print('10/14: Counts before each edge (to match findNodes.sh')
        # put the counts before each unique edge, transpose it, then sort (and
        # reverse) the list, so the output matches the previous findNodes.sh
        # script noinspection PyUnusedLocal
        results = sorted([' '.join(s) for
                          s in _np.asarray((counts, edges)).T])[::-1]

        if debug:
            print('11/14: Create array of data with counts at the beginning')
        # Create array of all the data, with counts at the beginning
        data = _np.c_[counts, _np.array([i.split(' ') for i in edges])]

        if debug:
            print('12/14: Sort data by counts')
        # sort by counts column
        sort_idx = _np.lexsort((data[:, 0],))[::-1]
        data = data[sort_idx]

        if debug:
            print('13/14: Find number of nodes in data')
        # Find number of nodes by taking the length of the data
        num_nodes = len(data)

        if debug:
            print('14/14: Find mean node connectivity')
        # Find average node connectivity
        mean_k = data.astype('float32')[:, 0].mean()

    if save_output:
        _np.savetxt(node_output,
                    data,
                    fmt='%s',
                    header="Python findNodes run at {0}\n\n"
                          "List of nodes in input skeleton \"{1}\"\n"
                          " from working directory \"{2}\"\n"
                          "\n"
                          "{3} total edges in skeleton\n"
                          "{4} total nodes in skeleton\n"
                          "\n"
                          "of\n"
                          "edges x\t\t\ty\t\t\t\tz\t\t\tThickness\n"
                          "----------------------------------------------"
                          "---------".format(_dt.strftime(_dt.now(),
                                                         "%A - %b %d, "
                                                         "%Y - %I:%M:%S %p"),
                                             os.path.basename(input_fname),
                                             dir_name,
                                             num_edges,
                                             num_nodes),
                    delimiter='\t',
                    comments='## ')

    return data.astype(return_type), num_edges, num_nodes, mean_k


def calc_skel_params(num_edges,
                     num_nodes,
                     volume,
                     mean_k,
                     topo_length,
                     perc_deg_1_perc,
                     perc_deg_5_longest):
    """
    Given the input parameters, output a dataframe with all the values
    that we calculate for a skeleton analysis

    Parameters
    ----------
    num_edges: int
        number of edges (from `find_nodes``)
    num_nodes: int
        number of nodes (from ``find_nodes``)
    volume: float
        total cathode volume used for calculating volume densities for nodes a
        edges
    mean_k: float
        average node degree (from ``find_nodes``)
    topo_length: float
        average topological length from (``analyze_avizo_skel_csv``)
    perc_deg_1_perc: float
        percolation degree based off of 1% change (from
        ``analyze_avizo_skel_csv``)
    perc_deg_5_longest: float
        percolation degree based off of 5 longest graphs(from
        ``analyze_avizo_skel_csv``)

    Returns
    -------
    df: :class:`pandas.DataFrame`
        DataFrame containing all the parameters
    """
    df = _pd.DataFrame(columns=['E',
                                'N',
                                'E/V',
                                'N/V',
                                'mean_k',
                                'topo_l',
                                'perc_d_1_perc',
                                'perc_d_5_longest'])
    df.loc[len(df)] = [num_edges,
                       num_nodes,
                       num_edges / volume,
                       num_nodes / volume,
                       mean_k,
                       topo_length,
                       perc_deg_1_perc,
                       perc_deg_5_longest]

    return df


def analyze_avizo_skel_csv(input_fname,
                           num_nodes):
    """
    Analyze a spatial graph statistics csv file to find the skeleton's
    percolation degree and average topological length

    Parameters
    ----------
    input_fname: str
        input csv file to analyze
    num_nodes: int
        number of nodes in the network, which will be used to calculate the
        average topological length

    Returns
    -------
    perc_degree_1_perc: float
        percolation degree (defined by 1% change)
    perc_degree_5_longest: float
        percolation degree (defined as cumulative length fraction included in
        the first five graphs
    topo_length: float
        topological length (total length divided by number of nodes)
    """
    df = _pd.read_csv(input_fname, skiprows=1)

    # Slice the dataframe to only include the rows before the
    # 'Total' row
    df = df.ix[:(df['Graph ID'] == 'Total').idxmax() - 1]

    # Set the index to the Graph ID column
    df = df.set_index('Graph ID')
    df.index.name = None

    # Sort by total length (descending)
    df = df.sort_values(by=['Total Length'], ascending=False)

    # Get total sum of network length:
    tot_length = df['Total Length'].sum()

    # Create fraction of total length and cumulative length columns:
    df['% total length'] = df['Total Length'] / tot_length
    df['Cum Length'] = df['Total Length'].cumsum()

    perc_degree_5_longest = df['% total length'].iloc[:5].sum()

    # Calculate percolation degree by finding the first graph  that
    # adds less than 1% to the total network length, when summed
    # cumulatively. perc_degree is the fraction of total network
    # length included at this point:

    try:
        perc_degree_1_perc = float(df['Cum Length'][
                          df['% total length'] < 0.01][0] / tot_length)
    except IndexError as _:
        perc_degree_1_perc = 1.0

    # Topological length is total network length divided by number of nodes;
    topo_length = tot_length / num_nodes

    return perc_degree_1_perc, perc_degree_5_longest, topo_length


def bootstrap_skel_stats(mv3d_pattern,
                         csv_pattern=None,
                         n_bootstrap=100000,
                         volume=None,
                         save_output=False,
                         data_output_fname=None,
                         err_output_fname=None,
                         recursive=True,
                         debug=False):
    """
    Calculate errors for skeleton statistics, as output by the Avizo
    subsampling script. Operates on many .mv3d skeleton files

    Parameters
    ----------
    mv3d_pattern: str
        glob pattern to grab mv3d files to process from output of subvolume
        Avizo scripts. Based off of these filenames, corresponding .csv
        files containing the spatial graph statistics will be accessed as well.
        If the mv3d files are named:
        ``YMdA-01_labels.view.LSM.skel.am.subvolSkel.*.mv3d``
        The following pattern will be used to glob for the Spatial graph
        stats: ``YMdA-01_labels.view.LSM.skel.am.subvolSkel.*.csv``
    csv_pattern: :class:`str` or :data:`None`
        glob pattern to grab csv files to process from Avizo spatial graph
        output. If None, will attempt a calculation from the ``mv3d_pattern``
        value as described above
    n_bootstrap: int
        number of bootstrap samples to use when calculating confidence
        intervals
    volume: :data:`None` or :ref:`number <numbers>`
        volume of analyzed data cube. If this is given, data will be
        returned with nodes/volume and edges/volume given, in addition to
        the absolute values
    save_output: bool
        switch to control whether or not the output is written directly to a
        CSV file in the current directory
    data_output_fname: :data:`None` or :class:`str`
        filename to use when saving the data output; if None, an appropriate
        string will be built from the input ``pattern``
    err_output_fname: :data:`None` or :class:`str`
        filename to use when saving the error output; if None, an appropriate
        string will be built from the input ``pattern``
    recursive: bool
        switch to control if globbing is done recursively
    debug: bool
        switch to control premature returning of the data DataFrame for
        analysis

    Returns
    -------
    data_df: :class:`pandas.DataFrame`
        Dataframe with data from subvolume statistic calculations

    error_df: :class:`~pandas.DataFrame`
        Dataframe with low and high errors calculated using n_bootstrap
        samples
    """
    mv3d_filelist = glob.glob(mv3d_pattern, recursive=recursive)

    if len(mv3d_filelist) == 0:
        raise ValueError("Did not find any .mv3d files!")

    # Find index of second occurrence of a dot
    if csv_pattern is None:
        second_dot = mv3d_pattern.rindex('.', 0, mv3d_pattern.rindex('.') - 1)
        csv_pattern = mv3d_pattern[:second_dot] + '.*csv'
    csv_filelist = glob.glob(csv_pattern, recursive=recursive)

    # Control switching of csv calculations
    run_csv = False
    if len(csv_filelist) > 0:
        # print(len(csv_filelist))
        run_csv = True
        if len(csv_filelist) != len(mv3d_filelist):
            print("mv3d pattern: " + mv3d_pattern)
            print("csv pattern: " + csv_pattern)
            raise ValueError("mv3d and csv filelists were different lengths")

    if save_output and data_output_fname is None:
        data_output_fname = mv3d_pattern[
                            :-1 * mv3d_pattern[::-1].index('*') - 1] + \
                            'bootstrap_data.csv'
    if save_output and err_output_fname is None:
        err_output_fname = mv3d_pattern[
                           :-1 * mv3d_pattern[::-1].index('*') - 1] + \
                           'bootstrap_errors.csv'

    # Blank results dataframe:
    if volume:
        # Include the volume normalized data, if volume given
        data_df = _pd.DataFrame(columns=['E', 'E/V', 'N', 'N/V', 'mean_k'])
    else:
        data_df = _pd.DataFrame(columns=['E', 'N', 'mean_k'])

    # Loop over all the mv3d files and add the results from find_nodes to
    # the data frame
    bar = tqdm(mv3d_filelist, desc='Running find_nodes')
    for mv3d_file in bar:
        data_df = data_df.append(
            dict(list(zip(['E', 'N', 'mean_k'],
                          list(
                              find_nodes(mv3d_file, save_output=False)[1:])))),
            ignore_index=True)
        if volume:
            data_df['N/V'] = data_df['N'] / volume
            data_df['E/V'] = data_df['E'] / volume

        bar.set_description('Running find_nodes on ' +
                            os.path.basename(mv3d_file))

    if run_csv:
        # Add columns of interest to the dataframe
        data_df["perc_deg"] = ""
        data_df["topo_length"] = ""

        # Loop over all the mv3d files and add the results from find_nodes to
        # the data frame
        bar = tqdm(csv_filelist, desc='Analyzing CSV files')
        for i, csv_file in enumerate(bar):
            perc_degree, _, topo_length = analyze_avizo_skel_csv(csv_file,
                                                                 data_df[
                                                                     'N'][i])
            # Set percolation degree within data_df
            data_df.set_value(i, 'perc_deg', perc_degree)

            # Topological length is total network length divided by number
            # of nodes; add to data_df
            data_df.set_value(i, 'topo_length', topo_length)

    if debug:
        return data_df

    error_df = calculate_errors(data_df, n_bootstrap)

    if save_output:
        data_df.to_csv(path_or_buf=data_output_fname)
        print(("Data output saved to {}".format(data_output_fname)))
        error_df.to_csv(path_or_buf=err_output_fname)
        print(("Error output saved to {}".format(err_output_fname)))

    return data_df, error_df
