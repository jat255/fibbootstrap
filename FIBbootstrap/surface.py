# Copyright 2016 Joshua Taillon
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import pandas as _pd
from .utils import calculate_errors
import glob
import numpy as _np

__all__ = ['process_surface_stats_lsm_ysz',
           'process_surface_stats_lscf_gdc']


def process_surface_stats_lsm_ysz(pattern,
                                  n_bootstrap=100000,
                                  save_output=False,
                                  output_fname="subvolume_errors.csv",
                                  recursive=False):
    """
    Calculate errors for surface statistics, as output by the Avizo
    subsampling script. This version operates on LSM-YSZ material names

    Parameters
    ----------
    pattern: str
        glob pattern to grab csv files to process from output of subvolume
        Avizo scripts
    n_bootstrap: int
        number of bootstrap samples to use when calculating confidence
        intervals
    save_output: bool
        switch to control whether or not the output is written directly to a
        CSV file in the current directory
    output_fname: str
        filename to use when saving the output
    recursive: bool
        switch to control if globbing is done recursively or not

    Returns
    -------
    error_df: ~pandas.DataFrame
        Dataframe with low and high errors calculated using n_bootstrap
        samples
    """
    filelist = glob.glob(pattern, recursive=recursive)

    if len(filelist) == 0:
        raise ValueError("Did not find any .csv files to process!")

    # Blank results dataframe:
    data_df = _pd.DataFrame()

    for i, file_ in enumerate(filelist):
        # Read data:
        tmp_df = _pd.read_csv(file_, header=0, skiprows=1)

        # Delete unwanted columns:
        tmp_df = tmp_df.drop('Closedness', axis=1).drop('Triangles', axis=1)

        # Filter out anything that's not one of our materials:
        PRE_df = tmp_df[tmp_df['Material'].isin(['PRE','Pore'])] \
            .drop('Material', axis=1).reset_index(drop=True)
        LSM_df = tmp_df[tmp_df['Material'].isin(['LSM'])] \
            .drop('Material', axis=1).reset_index(drop=True)
        YSZ_df = tmp_df[tmp_df['Material'].isin(['YSZ'])] \
            .drop('Material', axis=1).reset_index(drop=True)

        # set columns to be useful names
        PRE_df.columns = ['PRE_A', 'PRE_V']
        LSM_df.columns = ['LSM_A', 'LSM_V']
        YSZ_df.columns = ['YSZ_A', 'YSZ_V']

        # Concatenate all three columns into one dataframe (with one row)
        subvol_df = _pd.concat([PRE_df, LSM_df, YSZ_df],
                               axis=1,
                               ignore_index=False)

        # Add this row to the results dataframe
        data_df = data_df.append(subvol_df, ignore_index=True)

    # Calculate various properties:
    data_df['Total_V'] = data_df['PRE_V'] + data_df['LSM_V'] + data_df['YSZ_V']
    data_df['Total_solid_V'] = data_df['LSM_V'] + data_df['YSZ_V']

    data_df['PRE_volNormSA'] = data_df['PRE_A'].div(data_df['PRE_V'])
    data_df['LSM_volNormSA'] = data_df['LSM_A'].div(data_df['LSM_V'])
    data_df['YSZ_volNormSA'] = data_df['YSZ_A'].div(data_df['YSZ_V'])

    data_df['PRE_BET_d'] = 6 * data_df['PRE_V'].div(data_df['PRE_A'])
    data_df['LSM_BET_d'] = 6 * data_df['LSM_V'].div(data_df['LSM_A'])
    data_df['YSZ_BET_d'] = 6 * data_df['YSZ_V'].div(data_df['YSZ_A'])

    data_df['PRE_volFrac'] = data_df['PRE_V'].div(data_df['Total_V'])
    data_df['LSM_volFrac'] = data_df['LSM_V'].div(data_df['Total_V'])
    data_df['YSZ_volFrac'] = data_df['YSZ_V'].div(data_df['Total_V'])

    data_df['LSM_solVolFrac'] = data_df['LSM_V'].div(data_df['Total_solid_V'])
    data_df['YSZ_solVolFrac'] = data_df['YSZ_V'].div(data_df['Total_solid_V'])

    output_df = data_df[['PRE_volNormSA',
                         'LSM_volNormSA',
                         'YSZ_volNormSA',
                         'PRE_BET_d',
                         'LSM_BET_d',
                         'YSZ_BET_d',
                         'PRE_volFrac',
                         'LSM_volFrac',
                         'YSZ_volFrac',
                         'LSM_solVolFrac',
                         'YSZ_solVolFrac']]

    error_df = calculate_errors(output_df, n_bootstrap)

    if save_output:
        error_df.to_csv(path_or_buf=output_fname)
        print("Output saved to {}".format(output_fname))

    return error_df


def process_surface_stats_lscf_gdc(pattern,
                                   n_bootstrap=100000,
                                   save_output=False,
                                   output_fname="subvolume_errors.csv",
                                   recursive=True,
                                   phase1=True,
                                   cr_phase=False):
    """
    Calculate errors for surface statistics, as output by the Avizo
    subsampling script. This version operates on LSCF-GDC material names

    Parameters
    ----------
    pattern: str
        glob pattern to grab csv files to process from output of subvolume
        Avizo scripts
    n_bootstrap: int
        number of bootstrap samples to use when calculating confidence
        intervals
    save_output: bool
        switch to control whether or not the output is written directly to a
        CSV file in the current directory
    output_fname: str
        filename to use when saving the output
    recursive: bool
        switch to control if globbing is done recursively or not
    phase1: bool
        switch to control if error calculation is performed on "phase 1"
    cr_phase: bool
        switch to control if error calculation is performed on "Cr_phase"

    Returns
    -------
    error_df: ~pandas.DataFrame
        Dataframe with low and high errors calculated using n_bootstrap
        samples
    """
    filelist = glob.glob(pattern, recursive=recursive)

    if len(filelist) == 0:
        raise ValueError("Did not find any .csv files to process!")

    # Blank results dataframe:
    data_df = _pd.DataFrame()

    for i, file_ in enumerate(filelist):
        # Read data:
        tmp_df = _pd.read_csv(file_, header=0, skiprows=1)

        # Delete unwanted columns:
        tmp_df = tmp_df.drop('Closedness', axis=1).drop('Triangles', axis=1)

        # Filter out anything that's not one of our materials:
        PRE_df = tmp_df[tmp_df['Material'].isin(['PRE', 'Pore'])] \
            .drop('Material', axis=1).reset_index(drop=True)
        LSCF_df = tmp_df[tmp_df['Material'].isin(['LSCF'])] \
            .drop('Material', axis=1).reset_index(drop=True)
        GDC_df = tmp_df[tmp_df['Material'].isin(['GDC'])] \
            .drop('Material', axis=1).reset_index(drop=True)
        if phase1:
            phase1_df = tmp_df[tmp_df['Material'].isin(['Phase1', 'phase1'])] \
                .drop('Material', axis=1).reset_index(drop=True)
        if cr_phase:
            cr_phase_df = tmp_df[tmp_df['Material'].isin([
                'Cr_Phase', 'Cr_Phase2', 'Cr_phase', 'cr_phase'])] \
                .drop('Material', axis=1).reset_index(drop=True)

        # set columns to be useful names
        PRE_df.columns = ['PRE_A', 'PRE_V']
        LSCF_df.columns = ['LSCF_A', 'LSCF_V']
        GDC_df.columns = ['GDC_A', 'GDC_V']
        if phase1:
            phase1_df.columns = ['Ph1_A', 'Ph1_V']
        if cr_phase:
            cr_phase_df.columns = ['Cr_A', 'Cr_V']

        # Concatenate all three columns into one dataframe (with one row)
        cols = [PRE_df, LSCF_df, GDC_df]
        if phase1:
            cols.append(phase1_df)
        if cr_phase:
            cols.append(cr_phase_df)

        subvol_df = _pd.concat(cols,
                               axis=1,
                               ignore_index=False)

        # Add this row to the results dataframe
        data_df = data_df.append(subvol_df, ignore_index=True)

    # Calculate total surface volume:
    data_df['Total_V'] = data_df['PRE_V'] + \
                         data_df['LSCF_V'] + \
                         data_df['GDC_V']
    if phase1:
        data_df['Ph1_A'] = data_df['Ph1_A'].fillna(0)
        data_df['Ph1_V'] = data_df['Ph1_V'].fillna(0)
        data_df['Total_V'] += data_df['Ph1_V']
    if cr_phase:
        data_df['Cr_A'] = data_df['Cr_A'].fillna(0)
        data_df['Cr_V'] = data_df['Cr_V'].fillna(0)
        data_df['Total_V'] += data_df['Cr_V']

    # Total volume of original solids:
    data_df['Total_solid_V'] = data_df['LSCF_V'] + \
                               data_df['GDC_V']

    # Volume normalized surface areas
    data_df['PRE_volNormSA'] = data_df['PRE_A'].div(data_df['PRE_V'])
    data_df['LSCF_volNormSA'] = data_df['LSCF_A'].div(data_df['LSCF_V'])
    data_df['GDC_volNormSA'] = data_df['GDC_A'].div(data_df['GDC_V'])
    if phase1:
        data_df['Ph1_volNormSA'] = data_df['Ph1_A'].div(data_df['Ph1_V'])
        data_df['Ph1_volNormSA'] = data_df['Ph1_volNormSA'].replace(_np.inf, 0)
        data_df['Ph1_volNormSA'] = data_df['Ph1_volNormSA'].fillna(0)
    if cr_phase:
        data_df['Cr_volNormSA'] = data_df['Cr_A'].div(data_df['Cr_V'])
        data_df['Cr_volNormSA'] = data_df['Cr_volNormSA'].replace(_np.inf, 0)
        data_df['Cr_volNormSA'] = data_df['Cr_volNormSA'].fillna(0)

    # BET particle sizes:
    data_df['PRE_BET_d'] = 6 * data_df['PRE_V'].div(data_df['PRE_A'])
    data_df['LSCF_BET_d'] = 6 * data_df['LSCF_V'].div(data_df['LSCF_A'])
    data_df['GDC_BET_d'] = 6 * data_df['GDC_V'].div(data_df['GDC_A'])
    if phase1:
        data_df['Ph1_BET_d'] = 6 * data_df['Ph1_V'].div(data_df['Ph1_A'])
        data_df['Ph1_BET_d'] = data_df['Ph1_BET_d'].fillna(0)
    if cr_phase:
        data_df['Cr_BET_d'] = 6 * data_df['Cr_V'].div(data_df['Cr_A'])
        data_df['Cr_BET_d'] = data_df['Cr_BET_d'].fillna(0)

    # Volume fractions
    data_df['PRE_volFrac'] = data_df['PRE_V'].div(data_df['Total_V'])
    data_df['LSCF_volFrac'] = data_df['LSCF_V'].div(data_df['Total_V'])
    data_df['GDC_volFrac'] = data_df['GDC_V'].div(data_df['Total_V'])
    if phase1:
        data_df['Ph1_volFrac'] = data_df['Ph1_V'].div(data_df['Total_V'])
    if cr_phase:
        data_df['Cr_volFrac'] = data_df['Cr_V'].div(data_df['Total_V'])

    # Original material solid fractions:
    data_df['LSCF_solVolFrac'] = data_df['LSCF_V'].div(data_df[
                                                           'Total_solid_V'])
    data_df['GDC_solVolFrac'] = data_df['GDC_V'].div(data_df['Total_solid_V'])

    # Setup columns for output dataframe
    cols = ['PRE_volNormSA',
            'LSCF_volNormSA',
            'GDC_volNormSA',
            'PRE_BET_d',
            'LSCF_BET_d',
            'GDC_BET_d',
            'PRE_volFrac',
            'LSCF_volFrac',
            'GDC_volFrac',
            'LSCF_solVolFrac',
            'GDC_solVolFrac']
    # Add Phase1 and Cr_phase, if needed:
    if phase1:
        for i in ['Ph1_volNormSA', 'Ph1_BET_d', 'Ph1_volFrac']:
            cols.append(i)
    if cr_phase:
        for i in ['Cr_volNormSA', 'Cr_BET_d', 'Cr_volFrac']:
            cols.append(i)

    output_df = data_df[cols]

    error_df = calculate_errors(output_df, n_bootstrap)

    if save_output:
        error_df.to_csv(path_or_buf=output_fname)
        print("Output saved to {}".format(output_fname))

    return error_df
