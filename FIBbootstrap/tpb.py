import os
import numpy as _np
import pandas as _pd
from scipy.spatial.distance import pdist, squareform
import statsmodels.api as _sm
from tqdm import tqdm
from .utils import calculate_errors

__all__ = ['bootstrap_tpb_stats',
           'read_mv3d',
           'write_mv3d',
           'crop_tpb_data',
           'get_bb_from_data',
           'bb_volume',
           'get_random_subvolume',
           'path_length',
           'network_length',
           'split_paths_in_network',
           'animate_cropped_data',
           'get_bb_lines',
           'get_box_and_corners',
           'calc_tpb_params'
           ]


# noinspection PyUnusedLocal
def bootstrap_tpb_stats(inputs_dict=None,
                        box_size=4000,
                        n_volumes=500,
                        n_bootstrap=100000,
                        save_output=False,
                        data_output_fname=None,
                        err_output_fname=None,
                        output_avg=False):
    """
    Calculate statistics various TPB properties (and their errors) using a
    random subvolume sampling method. Total TPB length (for the subvolume),
    TPB density, and average length of a TPB path will be calculated.

    Parameters
    ----------
    inputs_dict: dict
        dictionary of values describing the input data. Keys should be
        labels for a particular set of TPB paths, while the values should be
        filenames for mv3d skeleton files of the TPB paths.
    box_size: float
        length of edge of cube used to define the subsampled volumes. The
        total volume sampled in each trial will be box_size**3, and the
        boxes will be selected randomly throughout the volume of data
    n_volumes: int
        number of subvolumes to sample from the volume (usually ~500 or so)
    n_bootstrap: int
        number of bootstrap samples to use when calculating confidence
        intervals
    save_output: bool
        switch to control whether or not the bootstrap data and error output is
        written directly to a CSV file in the current directory
    data_output_fname: str
        filename to use when saving the data output
    err_output_fname: str
        filename to use when saving the error output
    output_avg: bool
        switch to control whether "Avg TPB path length" will be calculated

    Returns
    -------
    data_df: :py:class:`~pandas.DataFrame`
        Dataframe with data from subvolume statistic calculations
    error_df: :py:class:`~pandas.DataFrame`
        Dataframe with low and high errors (and std. dev.) calculated using
        n_bootstrap samples

    Example
    -------

    >>> from FIBbootstrap.tpb import bootstrap_tpb_stats
    >>> inputs_dict = {
    ...     'active':'smoothActive.savg.mv3d',
    ...     'inactive':'smoothInactive.savg.mv3d',
    ...     'unknown':'smoothUnknown.savg.mv3d'}
    >>> data_out, \\
    ... error_out = bootstrap_tpb_stats(inputs_dict,
    ...                                 n_volumes=500,
    ...                                 box_size=4000,
    ...                                 save_output=False,
    ...                                 data_output_fname='data_N500_s4000.csv',
    ...                                 err_output_fname='errors_N500_s4000.csv',
    ...                                 output_avg=False)

    """
    # Create descriptive loading bar
    loading_bar = tqdm(inputs_dict.items(), desc='Loading skeleton file')

    # Empty dataframe for holding data
    overall_results = _pd.DataFrame()

    # Loop through the input dictionary, accessing each file
    for k, v in loading_bar:
        loading_bar.set_description('Loading skeleton file {}'.format(v))
        loading_bar.update()
        # Get data and some stats from the mv3d TPB network file
        data, num_lines, num_points = read_mv3d(v)

        # Infer appropriate bounding box from the data
        bb = get_bb_from_data(data)

        if output_avg:
            data_cols = ["_totL", "_avgL", "_TPBdens"]
        else:
            data_cols = ["_totL", "_TPBdens"]

        # Create temporary data frame to hold data for this key
        res_df1 = _pd.DataFrame(columns=[k + i for i in data_cols])

        # Descriptive progress bar for subvolume sampling
        # Loop through n_volumes different subvolumes
        subvol_bar = tqdm(range(n_volumes), desc='Calculating on {}'.format(v))
        for n in subvol_bar:
            # Get a random position within the data, crop the TPB network,
            # and try to split outlying large jumps (where the TPB path
            # intersects a face of the subvolume)
            box_start, box_end = get_random_subvolume(bb, box_size)
            cropped_data = crop_tpb_data(data, box_start, box_end)
            try:
                split_cropped_data = split_paths_in_network(cropped_data)
            except ValueError as e:
                # If the outlier detection didn't work, just default to the
                # regular cropped data
                # print("Outlier detection failed for {0} "
                #       "in iteration {1}".format(v, n))
                split_cropped_data = cropped_data

            # Gather the data about this cropped network:
            lengths = network_length(split_cropped_data)
            total_length_nm = lengths[0]
            total_length_um = total_length_nm / 1000
            avg_length_nm = lengths[1].mean()
            avg_length_um = avg_length_nm / 1000
            bb_vol_nm = box_size**3
            bb_vol_um = bb_vol_nm / (1000**3)

            density_nm = total_length_nm / bb_vol_nm
            density_um = total_length_um / bb_vol_um

            if output_avg:
                data_results = [total_length_um, avg_length_um, density_um]
            else:
                data_results = [total_length_um, density_um]

            # Add each of the above values to a temporary dataframe (microns)
            df1 = _pd.DataFrame(dict(zip([k + i for i in data_cols],
                                         data_results)),
                                index=[0])

            # Append this set of values to the "inner" results list as
            # another row. After this loop is finished, there should be
            # n_volumes rows in res_df1
            res_df1 = _pd.concat([res_df1, df1], ignore_index=True)

        # Build an "outer" results dataframe by adding the three columns
        # from the inner results to the overall results dataframe
        overall_results = _pd.concat([overall_results, res_df1],
                                     axis=1)

    # Calculate actual errors and save data
    data_df = overall_results
    error_df = calculate_errors(overall_results, n_bootstrap)

    if save_output:
        data_df.to_csv(path_or_buf=data_output_fname)
        print("Data output saved to {}".format(data_output_fname))
        error_df.to_csv(path_or_buf=err_output_fname)
        print("Error output saved to {}".format(err_output_fname))

    return data_df, error_df


# noinspection PyUnusedLocal
def read_mv3d(filename):
    """
    Get number of lines and points, as well as the 3d data contained withing
    an MV3D network file

    Parameters
    ----------
    filename: str
        Name of ``.mv3d`` file to open

    Returns
    -------
    data: :py:class:`~numpy.ndarray`
        (N x 4) numpy array containing the index, x, y, and z coordinates of
        each point within the network (the last value, ``d`` is discarded)

    num_lines: :class:`int`
        number of lines contained in the network (read from line 2 of the file)

    num_points: :class:`int`
        number of points contained in the network (read from line 3 of the
        file)
    """
    lines = []

    # Open file, discard first line, and return the remaining ones into a list
    with open(filename, 'r') as f:
        _ = f.readline()
        lines = f.readlines()

    # Remove newlines and tab characters from the text within `lines`
    for i, l in enumerate(lines):
        lines[i] = l.strip('\n').replace('\t', ' ')

    # Get number of lines and points from the file
    num_lines = int(lines[0].split(' ')[-1])
    num_points = int(lines[1].split(' ')[-1])

    # Remove blank lines and lines starting with '#' (header lines)
    lines1 = [x for x in lines if x != '' and not x.startswith('#')]

    # Split each string into many strings so it can be read into a numpy array
    lines2 = [x.split()[:-1] for x in lines1]

    # Create numpy data array
    data = _np.array(lines2,
                     dtype='float32')

    return data, num_lines, num_points


def get_random_subvolume(bb, size):
    """
    Given a particular bounding box and subvolume size, return
    two lists with x, y, z coordinates of lower and upper corners of a random
    subvolume withing the bounding box. Returned subvolume will be completely
    enclosed within bb (i.e. the maximum position for the lower bound of the
    cube x value will be bb[0] - size).

    Parameters
    ----------
    bb: tuple
        tuple of length two, each term should be an :term:`iterable` of length
        three with the minimum (position 0) and maximum (position 1) bounding
        box coordinate in each dimension from which to take a random volume;
        returned volume will be in the range
        [min_x, max_x], [min_y, max_y], and [min_z, max_z]

    size: :ref:`number <numbers>`
        size of cube to return (single edge length, so total volume enclosed
        will be size**3)

    Returns
    -------
    box_start: :class:`list`
        lower bound corner of the subvolume box (x, y, and z)
    box_end: :class:`list`
        upper bound corner of the subvolume box (x, y, and z
    """
    import random
    bb_min, bb_max = bb
    box_start = [random.uniform(bb_min[i], x - size) for i, x in enumerate(
        bb_max)]
    box_end = [x + size for x in box_start]

    return box_start, box_end


def split_paths_in_network(data,
                           threshold=0.2):
    """
    Given an array of data, try to detect the paths that are on the edge of
    the volume and split the single path (containing a sudden long jump)
    into multiple paths.

    Parameters
    ----------
    data: :py:class:`~numpy.ndarray`
        array of data, like that returned by
        :py:func:`~FIBbootstrap.tpb.read_mv3d` or
        :py:func:`~FIBbootstrap.tpb.crop_tpb_data`
    threshold: float
        weighting threshold in the range [0, 1] to help determine what is an
        outlier. All the data in each path is fit by a robust linear model,
        so any length values that are significantly different should have
        a weight << 1.0. Set this value higher to find more outliers, and thus
        split more paths. Set it lower to be more conservative.

    Returns
    -------
    split_data: :py:class:`~numpy.ndarray`
        copy of the original data, but with paths split at points that caused
        particularly large jumps from point to point
    """
    df = _pd.DataFrame(data, columns=['id', 'x', 'y', 'z'])
    next_id = df['id'].max() + 1

    for idx, g_df in df.groupby('id'):
        # find pairwise distance and calculate an array of distances between
        # subsequent nodes in the path (`step_lengths`)
        pdist_vec = pdist(g_df[['x', 'y', 'z']], metric='euclidean')
        dist_mat = squareform(pdist_vec)
        step_lengths = _np.diagonal(dist_mat, offset=1)

        # create robust linear model to estimate function describing data
        rlm_model = _sm.RLM(step_lengths,
                            range(len(step_lengths)),
                            M=_sm.robust.norms.HuberT())
        rlm_results = rlm_model.fit()

        # outliers are where the model does not describe the data well
        # (weight is below some threshold)
        jumps = _np.where(_np.array(rlm_results.weights) < threshold)[0]

        # if there were large jumps in the data:
        if len(jumps) > 0:
            # split the current grouped dataframe at the jump location
            split_df_list = _np.split(g_df, [x + 1 for x in jumps])

            # change the id of split dataframes (except for the first one) and
            # increment the next_id counter
            for d in split_df_list[1:]:
                d['id'] = int(next_id)
                next_id += 1

            # remove the current (unsplit) segment from the total dataframe
            df = df.query('id != {}'.format(idx))

            # append the split segments to the end of the overall dataframe
            for d in split_df_list:
                df = df.append(d)

    split_data = df.as_matrix()
    return split_data


def write_mv3d(fname, data, d=0, overwrite=True):
    """
    Output path data to an mv3d file, which can be read by Avizo (and other
    software)

    Parameters
    ----------
    fname: str
        Filename to which to write; will be overwritten if it exists (by
        default)
    data: :py:class:`~numpy.ndarray`
        array containing network (spatial graph) data in the same format as
        output by :py:func:`~FIBbootstrap.tpb.read_mv3d` or
        :py:func:`~FIBbootstrap.tpb.crop_tpb_data`
    d: :py:class:`~numpy.ndarray`
        the value to be written in the 'thickness' column of the mv3d file. Can
        be used to tag files with a scalar value. If a single number is
        given instead of an array, that number will be used for every point.
        If a numpy array (the same length as the data), the values will be
        specific to each point. Standard pandas/numpy broadcasting rules apply
    overwrite: bool
        switch to control whether an existing file will be clobbered if it
        already exists
    """
    if os.path.exists(fname):
        if overwrite:
            print('Output file exists; overwriting...')
            pass
        else:
            raise IOError('Output file exists;'
                          'set overwrite=True to clobber the file')

    # Put data into a pandas dataframe for processing
    df = _pd.DataFrame(data, columns=['id', 'x', 'y', 'z'])
    # assign 'd' column for dataframe
    df['d'] = d

    # calculate the number of lines and points (to be written in the mv3d
    # header)
    num_lines = len(df.groupby('id'))
    num_points = len(df)

    # Disable some pandas warnings that are not relevant
    original_warn_setting = _pd.options.mode.chained_assignment
    _pd.options.mode.chained_assignment = None

    # Write mv3d header into fname
    with open(fname, 'w') as f:
        f.write('# MicroVisu3D file\n')
        f.write('# Number of lines   {}\n'.format(num_lines))
        f.write('# Number of points  {}\n'.format(num_points))
        f.write('# Number of inter.  0\n')
        f.write('#\n')
        f.write('# No		x		y		z		d\n')
        f.write('#\n')

    # Loop through the paths defined by 'id', reset their id number to an
    # incrementing counter, and then write the data to the file, placing a
    # space between each individual path within the network
    for i, (idx, group_df) in enumerate(df.groupby('id')):
        group_df['id'] = i
        with open(fname, 'a') as f:
            # write just the data to the file (no header or index info)
            group_df.to_csv(f,
                            float_format='%.6f',
                            sep='\t',
                            header=False,
                            index=False)
            f.write('\n')

    _pd.options.mode.chained_assignment = original_warn_setting


def crop_tpb_data(data, box_start, box_end):
    """
    Crop TPB data to only contain points within the box defined by the corners
    box_start and box_end

    Parameters
    ----------
    data: :py:class:`~numpy.ndarray`
        (N x 4) numpy array with TPB data (as loaded by
        :py:func:`~FIBbootstrap.tpb.read_mv3d`),
        including the first column containing the index

    box_start: list or :py:class:`~numpy.ndarray`
        x, y, z coordinates of lower bound corner to crop inside of

    box_end: list or :py:class:`~numpy.ndarray`
        x, y, z coordinates of upper bound corner to crop inside of

    Returns
    -------
    cropped_data: :py:class:`~numpy.ndarray`
        copy of the original data, containing only the points inside of the
        crop box
    """
    # want to find the rows of the data array to keep:
    # find where data is larger than the start, and less than the end
    comp = _np.logical_and(data[:, 1:] > box_start, data[:, 1:] < box_end)

    # Using _np.all with the axis=1 option tests just along rows
    all_true = _np.all(comp, axis=1)

    # Find the index of the rows that
    true_row_idx = _np.where(all_true)[0]

    # get only the data inside the box
    cropped_data = data[true_row_idx, :]

    return cropped_data


def path_length(path_df):
    """
    Calculate the length along a path.

    Parameters
    ----------
    path_df: ~pandas.DataFrame
        path_df should have 4 columns, 'id', 'x', 'y', 'z'. Using
        :py:func:`scipy.spatial.distance.pdist`,
        this method will calculate the sum of the distances between successive
        rows of (x, y, z) coordinates in the dataframe

    Returns
    -------
    length: float
        total sum of path defined by successive points in ``path_df``
    """
    # using just the x, y, z coordinates, calculate the condensed distance
    # matrix for the series of points (pdist will calculate the distance
    # between every point in the list, not just successive points)
    pdist_vec = pdist(path_df[['x', 'y', 'z']], metric='euclidean')

    # convert condensed distance matrix into a square distance matrix, which
    # holds the distance between points i and j at dist_mat[i, j]
    dist_mat = squareform(pdist_vec)

    # since we are interested in the distance between successive points,
    # the diagonal of dist_mat (offset by 1) holds all the individual lengths
    # we are interested in. _np.trace will sum these for us automatically
    length = _np.trace(dist_mat, offset=1)

    return length


def network_length(data):
    """
    Calculate the total length of a network that has been imported from
    the mv3d format

    Parameters
    ----------
    data: ~numpy.ndarray
        network data, in the format returned by
        :py:func:`~FIBbootstrap.tpb.read_mv3d` or
        :py:func:`~FIBbootstrap.tpb.crop_tpb_data`

    Returns
    -------
    lengths: :class:`~numpy.ndarray`
        lengths of each individual segment within the network
    tot_length: :class:`float`
        total length of network
    """
    # Convert numpy array network data into a pandas Dataframe
    df = _pd.DataFrame(data, columns=['id', 'x', 'y', 'z'])

    # Group the dataframe by segment id, so we can calculate individual lengths
    groupby_id = df.groupby(df['id'])

    # Apply the path_length function to each group, which will calculate the
    # summed Euclidean distance between each point in the segment
    lengths_series = groupby_id.apply(path_length)

    # Convert the resulting pandas Series into a numpy array
    lengths = lengths_series.as_matrix()

    # Calculate the total length of all the segments
    tot_length = lengths.sum()

    return tot_length, lengths


def get_box_and_corners(box_start, box_end):
    """
    Given two opposite corners of a 3D rectangle, find the necessary vectors
    that will allow for plotting of the edges of the rectangle and points at
    each of the corners using MayaVi

    Parameters
    ----------
    box_start: :term:`iterable`
        lower bounding corner of the rectangle (i.e. [x, y, z])
    box_end: :term:`iterable`
        upper bounding corner of the rectangle (i.e. [x, y, z])

    Returns
    -------
    x: :py:class:`~numpy.ndarray`
        array of x positions for :py:func:`mayavi.mlab.plot3d` for plotting
        edges of 3D rectangle
    y: :py:class:`~numpy.ndarray`
        array of y positions for :py:func:`~mayavi.mlab.plot3d` for plotting
        edges of 3D rectangle
    z: :py:class:`~numpy.ndarray`
        array of z positions for :py:func:`~mayavi.mlab.plot3d` for plotting
        edges of 3D rectangle
    x_p: :py:class:`~numpy.ndarray`
        array of x positions for :py:func:`~mayavi.mlab.plot3d` for plotting
        corners of 3D rectangle
    y_p: :py:class:`~numpy.ndarray`
        array of y positions for :py:func:`~mayavi.mlab.plot3d` for plotting
        corners of 3D rectangle
    z_p: :py:class:`~numpy.ndarray`
        array of z positions for :py:func:`~mayavi.mlab.plot3d` for plotting
        corners of 3D rectangle
    """
    c = _np.array([box_start, box_end])

    x = _np.hstack((c[0, 0], c[1, 0], c[1, 0], c[0, 0], c[0, 0],
                    c[0, 0], c[1, 0], c[1, 0], c[0, 0], c[0, 0],
                    c[1, 0], c[1, 0], c[1, 0], c[1, 0], c[0, 0],
                    c[0, 0]))
    y = _np.hstack((c[0, 1], c[0, 1], c[1, 1], c[1, 1], c[0, 1],
                    c[0, 1], c[0, 1], c[1, 1], c[1, 1], c[0, 1],
                    c[0, 1], c[0, 1], c[1, 1], c[1, 1], c[1, 1],
                    c[1, 1]))
    z = _np.hstack((c[0, 2].repeat(5), c[1, 2].repeat(5),
                    c[1, 2], c[0, 2], c[0, 2], c[1, 2], c[1, 2],
                    c[0, 2]))

    x_p = _np.hstack((c[0, 0].repeat(4),
                      c[1, 0].repeat(4)))
    y_p = _np.hstack((c[0, 1], c[0, 1], c[1, 1], c[1, 1],
                      c[1, 1], c[0, 1], c[1, 1], c[0, 1]))
    z_p = _np.hstack((c[0, 2], c[1, 2], c[1, 2], c[0, 2],
                      c[1, 2], c[0, 2], c[0, 2], c[1, 2]))

    return x, y, z, x_p, y_p, z_p


def get_bb_lines(bb):
    """
    Given a bounding box, find the necessary vectors that will allow for
    plotting of the edges of the bounding box using MayaVi

    Parameters
    ----------
    bb: :class:`tuple` of length 2
        tuple of length two, each term should be an :term:`iterable` of length
        three with the minimum (position 0) and maximum (position 1) bounding
        box coordinate in each dimension from which to take a random volume;
        returned volume will be in the range

    Returns
    -------
    x: :py:class:`~numpy.ndarray`
        array of x positions for :py:func:`mayavi.mlab.plot3d` for plotting
        edges of 3D rectangle
    y: :py:class:`~numpy.ndarray`
        array of y positions for :py:func:`~mayavi.mlab.plot3d` for plotting
        edges of 3D rectangle
    z: :py:class:`~numpy.ndarray`
        array of z positions for :py:func:`~mayavi.mlab.plot3d` for plotting
        edges of 3D rectangle
    """
    box_start = list(x for x in bb[0])
    box_end = list(x for x in bb[1])
    c = _np.array([box_start, box_end])

    x = _np.hstack((c[0, 0], c[1, 0], c[1, 0], c[0, 0], c[0, 0],
                    c[0, 0], c[1, 0], c[1, 0], c[0, 0], c[0, 0],
                    c[1, 0], c[1, 0], c[1, 0], c[1, 0], c[0, 0],
                    c[0, 0]))
    y = _np.hstack((c[0, 1], c[0, 1], c[1, 1], c[1, 1], c[0, 1],
                    c[0, 1], c[0, 1], c[1, 1], c[1, 1], c[0, 1],
                    c[0, 1], c[0, 1], c[1, 1], c[1, 1], c[1, 1],
                    c[1, 1]))
    z = _np.hstack((c[0, 2].repeat(5), c[1, 2].repeat(5),
                    c[1, 2], c[0, 2], c[0, 2], c[1, 2], c[1, 2],
                    c[0, 2]))

    return x, y, z


def get_bb_from_data(data_list):
    """
    Infer from the the existing data the extents of the bounding box

    Parameters
    ----------
    data_list: list
        list or array of numpy arrays with data (in the format returned by
        :py:func:`~FIBbootstrap.tpb.read_mv3d` or
        :py:func:`~FIBbootstrap.tpb.crop_tpb_data`.
        Expects 4 columns representing ['id', 'x', 'y', 'z'].

    Returns
    -------
    min_bb: :class:`tuple`
        list of x, y, z values containing the smallest coordinates in each
        dimension found in the data_list

    max_bb: :class:`tuple`
        list of x, y, z values containing the largest coordinates in each
        dimension found in the data_list
    """
    max_bb = tuple(_np.vstack(data_list).max(axis=0)[1:])
    min_bb = tuple(_np.vstack(data_list).min(axis=0)[1:])
    return min_bb, max_bb


def bb_volume(bb):
    """
    Return the volume enclosed by a bounding box.

    Parameters
    ----------
    bb: :class:`tuple` of length 2
        tuple of length two, each term should be an :term:`iterable` of length
        three with the minimum (position 0) and maximum (position 1) bounding
        box coordinate in each dimension

    Returns
    -------
    volume: float
        volume enclosed by the bounding box
    """
    min_bb, max_bb = bb

    volume = 1

    for i in range(3):
        volume *= (max_bb[i] - min_bb[i])

    return volume


# noinspection PyUnusedLocal
def animate_cropped_data(data_a,
                         data_i,
                         data_u,
                         size=4000,
                         subsample=1,
                         bb=None):
    """
    Plot a simple animation of the total TPB path network, as well as the
    network contained within a randomly cropped volume

    Parameters
    ----------
    data_a: :py:class:`~numpy.ndarray`
        network data, in the format returned by
        :py:func:`~FIBbootstrap.tpb.read_mv3d` or
        :py:func:`~FIBbootstrap.tpb.crop_tpb_data`;
        will be plotted in green (active)
    data_i: :py:class:`~numpy.ndarray`
        network data, in the format returned by
        :py:func:`~FIBbootstrap.tpb.read_mv3d` or
        :py:func:`~FIBbootstrap.tpb.crop_tpb_data`;
        will be plotted in red (inactive)
    data_u: :py:class:`~numpy.ndarray`
        network data, in the format returned by
        :py:func:`~FIBbootstrap.tpb.read_mv3d` or
        :py:func:`~FIBbootstrap.tpb.crop_tpb_data`;
        will be plotted in yellow (unknown)
    size: :ref:`number <numbers>`
        size of cube to return (single edge length, so total volume enclosed
        will be size**3)
    subsample: int
        factor by which to subsample the total tpb network. If > 1, it will
        speed up the plotting (may be helpful on lower-powered CPUs)
    bb: :data:`None` or :class:`tuple` of length 2
        tuple of length two, each term should be an :term:`iterable` of length
        three with the minimum (position 0) and maximum (position 1) bounding
        box coordinate in each dimension from which to take a random volume;
        returned volume will be in the range
        [min_x, max_x], [min_y, max_y], and [min_z, max_z]
        If None, the bounding box will be inferred from the supplied data
    """
    from mayavi import mlab

    green = (0.3, 0.7, 0.4)
    yellow = (0.9, 0.9, 0.5)
    red = (0.9, 0.2, 0.2)
    orange = (255.0 / 255, 165. / 255, 0)
    white = (1, 1, 1)

    if bb is None:
        bb = get_bb_from_data([data_a, data_i, data_u])
        min_bb, max_bb = bb
    else:
        min_bb, max_bb = bb

    x_a, y_a, z_a = data_a[::subsample, 1:].T
    x_i, y_i, z_i = data_i[::subsample, 1:].T
    x_u, y_u, z_u = data_u[::subsample, 1:].T

    f1 = mlab.figure(size=(500, 500))
    f2 = mlab.figure(size=(500, 500))

    # Plotting the subvolume box on both figures
    options = {'tube_radius': 50, 'tube_sides': 100}
    point_options = {'scale_factor': 200, 'color': (0, 0, 0)}
    box_start, box_end = get_random_subvolume(bb, size)
    x, y, z, x_p, y_p, z_p = get_box_and_corners(box_start, box_end)
    m_lines1 = mlab.plot3d(x, y, z, figure=f1, **options)
    m_points1 = mlab.points3d(x_p, y_p, z_p, figure=f1, **point_options)
    m_lines2 = mlab.plot3d(x, y, z, figure=f2, **options)
    m_points2 = mlab.points3d(x_p, y_p, z_p, figure=f2, **point_options)

    # Plotting the tpb points
    tpb_options = {'mode': 'point',
                   'scale_factor': 50,
                   'opacity': 0.5}
    x_tot_a, y_tot_a, z_tot_a = data_a[::subsample, 1:].T
    x_tot_i, y_tot_i, z_tot_i = data_i[::subsample, 1:].T
    x_tot_u, y_tot_u, z_tot_u = data_u[::subsample, 1:].T

    # plot total TPB network in figure 1
    m_tot_a = mlab.points3d(x_tot_a, y_tot_a, z_tot_a,
                            color=green, figure=f1, **tpb_options)
    m_tot_i = mlab.points3d(x_tot_i, y_tot_i, z_tot_i,
                            color=red, figure=f1, **tpb_options)
    m_tot_u = mlab.points3d(x_tot_u, y_tot_u, z_tot_u,
                            color=yellow, figure=f1, **tpb_options)

    # plot just the subsampled volume in figure 2
    m_a = mlab.points3d(x_a, y_a, z_a,
                        color=green, figure=f2, **tpb_options)
    m_i = mlab.points3d(x_i, y_i, z_i,
                        color=red, figure=f2, **tpb_options)
    m_u = mlab.points3d(x_u, y_u, z_u,
                        color=yellow, figure=f2, **tpb_options)

    # plot overall bounding box
    x_bb, y_bb, z_bb = get_bb_lines(bb)
    bb_lines1 = mlab.plot3d(x_bb, y_bb, z_bb,
                            color=orange,
                            tube_radius=20,
                            tube_sides=100,
                            figure=f1)
    bb_lines2 = mlab.plot3d(x_bb, y_bb, z_bb,
                            color=orange,
                            tube_radius=20,
                            tube_sides=100,
                            figure=f2)

    # noinspection PyUnusedLocal
    @mlab.animate(delay=2000)
    def anim():
        f = mlab.gcf()
        while True:
            box_inner_start, box_inner_end = get_random_subvolume(bb, size)

            # box_start, box_end = random_box(bb, size)
            cropped_a = crop_tpb_data(data_a, box_inner_start, box_inner_end)
            cropped_i = crop_tpb_data(data_i, box_inner_start, box_inner_end)
            cropped_u = crop_tpb_data(data_u, box_inner_start, box_inner_end)

            x_inner_a, y_inner_a, z_inner_a = cropped_a[:, 1:].T
            x_inner_i, y_inner_i, z_inner_i = cropped_i[:, 1:].T
            x_inner_u, y_inner_u, z_inner_u = cropped_u[:, 1:].T

            x_inner, y_inner, z_inner, x_inner_p, y_inner_p, z_inner_p = \
                get_box_and_corners(box_inner_start, box_inner_end)

            m_lines1.mlab_source.reset(x=x_inner, y=y_inner, z=z_inner)
            m_points1.mlab_source.reset(x=x_inner_p, y=y_inner_p, z=z_inner_p)
            m_lines2.mlab_source.reset(x=x_inner, y=y_inner, z=z_inner)
            m_points2.mlab_source.reset(x=x_inner_p, y=y_inner_p, z=z_inner_p)
            m_a.mlab_source.reset(x=x_inner_a, y=y_inner_a, z=z_inner_a)
            m_i.mlab_source.reset(x=x_inner_i, y=y_inner_i, z=z_inner_i)
            m_u.mlab_source.reset(x=x_inner_u, y=y_inner_u, z=z_inner_u)

            mlab.sync_camera(f1, f2)
            yield

    anim()
    mlab.show()


def calc_tpb_params(data_fname,
                    err_fname,
                    volume):
    """
    Given an output file from processOutput.sh (https://bitbucket.org/jat255/
    tpb-scripts/src/master/processOutput.sh), and an error output file from
    :py:func:`~FIBbootstrap.tpb.bootstrap_tpb_stats`, format the results
    into an easily digestible format and return as a
    :py:class:`~pandas.DataFrame`

    Parameters
    ----------
    data_fname: str
        Formatted data file output by the ``processOutput.sh`` script
    err_fname: str
        Error output created by
        :py:func:`~FIBbootstrap.tpb.bootstrap_tpb_stats`
    volume: float
        Cathode volume (used for calculating densities)

    Returns
    -------
    df: :py:class:`~pandas.DataFrame`
        DataFrame containing all the relevant TPB parameters, formatted
        nicely with errors, and ready to be written to a csv file
    """

    from uncertainties import ufloat

    def _correct_unknown(active, inactive, unknown):
        """
        Split the unknown portion of TPB into the active and inactive parts,
        using the existing ratio between active/inactive, using the formula:

        A' = A + A/(A+I)*U
        I' = I + I/(A+I)*U

        Parameters
        ----------
        active: float
            active TPB (density, length, percent, whatever...)
        inactive: float
            inactive TPB (density, length, percent, whatever...)
        unknown: float
            unknown TPB (density, length, percent, whatever...)

        Returns
        -------
        active_corr: float
            corrected active value
        inactive_corr: float
            corrected inactive value
        """
        active_corr = active + active / (active + inactive) * unknown
        inactive_corr = inactive + inactive / (active + inactive) * unknown

        return active_corr, inactive_corr

    # Read formatted output into list and remove some extraneous information
    with open(data_fname, 'r') as f:
        lines = f.readlines()
    lines = [x for x in lines if x != '\n']
    lines = [x.replace('\n', '').split(' ') for x in lines]

    # Parse list into dictionary structure
    d = {}
    for i in [0, 4] + \
            list(range(6, 10)) + \
            list(range(26, 34)) + \
            list(range(37, 45)) + \
            list(range(48, 56)) + \
            list(range(59, 67)):
        if len(lines[i]) == 2:
            (key, val) = lines[i]
        else:
            (key, *val) = lines[i]
        d[key] = val

    # Try to convert single items into floats for calculation
    for k, v in d.items():
        try:
            d[k] = float(v)
        except:
            pass

    raw_errors = _pd.read_csv(err_fname,
                              sep=',',
                              index_col=0).drop('---').iloc[:2]
    raw_errors = raw_errors.astype('float')
    raw_errors = raw_errors.T

    # Wrangle the raw error input into a more useful form
    errors = _pd.DataFrame(columns=['A -', 'A +', 'I -', 'I +', 'U -', 'U +'],
                           index=['Length', 'Density'])
    df = raw_errors
    errors['A -'].loc['Density'] = \
        df.loc[['_active_TPB' in x for x in df.index]].iloc[:, 0][0]
    errors['A +'].loc['Density'] = \
        df.loc[['_active_TPB' in x for x in df.index]].iloc[:, 1][0]
    errors['A -'].loc['Length'] = \
        df.loc[['_active_totL' in x for x in df.index]].iloc[:, 0][0]
    errors['A +'].loc['Length'] = \
        df.loc[['_active_totL' in x for x in df.index]].iloc[:, 1][0]

    errors['I -'].loc['Density'] = \
        df.loc[['_inactive_TPB' in x for x in df.index]].iloc[:, 0][0]
    errors['I +'].loc['Density'] = \
        df.loc[['_inactive_TPB' in x for x in df.index]].iloc[:, 1][0]
    errors['I -'].loc['Length'] = \
        df.loc[['_inactive_totL' in x for x in df.index]].iloc[:, 0][0]
    errors['I +'].loc['Length'] = \
        df.loc[['_inactive_totL' in x for x in df.index]].iloc[:, 1][0]

    errors['U -'].loc['Density'] = \
        df.loc[['_unknown_TPB' in x for x in df.index]].iloc[:, 0][0]
    errors['U +'].loc['Density'] = \
        df.loc[['_unknown_TPB' in x for x in df.index]].iloc[:, 1][0]
    errors['U -'].loc['Length'] = \
        df.loc[['_unknown_totL' in x for x in df.index]].iloc[:, 0][0]
    errors['U +'].loc['Length'] = \
        df.loc[['_unknown_totL' in x for x in df.index]].iloc[:, 1][0]

    # Read data from dictionary into dataframe and calculate percents
    raw_df = _pd.DataFrame(columns=['Active', 'Inactive', 'Unknown', 'All'],
                           index=['Length', 'Density', 'Percent'])
    df = raw_df
    for c in df.columns:
        df[c].loc['Length'] = float(d['sum_Length{}'.format(c)][0]) / 1000
        df[c].loc['Density'] = df[c].loc['Length'] / volume
    for c in df.columns:
        df[c].loc['Percent'] = df[c].loc['Length'] / df['All'].loc['Length']

    # Concatenate data and errors together
    raw_w_err = _pd.concat([raw_df, errors], axis=1)
    raw_w_err = raw_w_err.reindex_axis(['Active', 'A -', 'A +',
                                        'Inactive', 'I -', 'I +',
                                        'Unknown', 'U -', 'U +',
                                        'All'], axis=1)

    # Calculate errors for 'Percent' row of DataFrame
    df = raw_w_err
    a_neg = ufloat(df['Active'].loc['Density'], df['A -'].loc['Density'])
    a_pos = ufloat(df['Active'].loc['Density'], df['A +'].loc['Density'])
    i_neg = ufloat(df['Inactive'].loc['Density'], df['I -'].loc['Density'])
    i_pos = ufloat(df['Inactive'].loc['Density'], df['I +'].loc['Density'])
    u_neg = ufloat(df['Unknown'].loc['Density'], df['U -'].loc['Density'])
    u_pos = ufloat(df['Unknown'].loc['Density'], df['U +'].loc['Density'])

    ap_neg = a_neg / (a_neg + i_neg + u_neg)
    ap_pos = a_pos / (a_pos + i_pos + u_pos)
    ip_neg = i_neg / (a_neg + i_neg + u_neg)
    ip_pos = i_pos / (a_pos + i_pos + u_pos)
    up_neg = u_neg / (a_neg + i_neg + u_neg)
    up_pos = u_pos / (a_pos + i_pos + u_pos)

    df['A -'].loc['Percent'] = ap_neg.std_dev
    df['A +'].loc['Percent'] = ap_pos.std_dev
    df['I -'].loc['Percent'] = ip_neg.std_dev
    df['I +'].loc['Percent'] = ip_pos.std_dev
    df['U -'].loc['Percent'] = up_neg.std_dev
    df['U +'].loc['Percent'] = up_pos.std_dev

    # Convert raw dataframe into one with uncertainty values
    # Active
    col = 0
    act = {}
    val_dict = act
    for i in range(len(raw_w_err)):
        val_neg = ufloat(raw_w_err.iloc[i, col], raw_w_err.iloc[i, col + 1])
        val_pos = ufloat(raw_w_err.iloc[i, col], raw_w_err.iloc[i, col + 2])
        val_dict['{}_neg'.format(raw_w_err.index[i])] = val_neg
        val_dict['{}_pos'.format(raw_w_err.index[i])] = val_pos

    # Inactive
    col = 3
    inact = {}
    val_dict = inact
    for i in range(len(raw_w_err)):
        val_neg = ufloat(raw_w_err.iloc[i, col], raw_w_err.iloc[i, col + 1])
        val_pos = ufloat(raw_w_err.iloc[i, col], raw_w_err.iloc[i, col + 2])
        val_dict['{}_neg'.format(raw_w_err.index[i])] = val_neg
        val_dict['{}_pos'.format(raw_w_err.index[i])] = val_pos

    # Unknown
    col = 6
    unk = {}
    val_dict = unk
    for i in range(len(raw_w_err)):
        val_neg = ufloat(raw_w_err.iloc[i, col], raw_w_err.iloc[i, col + 1])
        val_pos = ufloat(raw_w_err.iloc[i, col], raw_w_err.iloc[i, col + 2])
        val_dict['{}_neg'.format(raw_w_err.index[i])] = val_neg
        val_dict['{}_pos'.format(raw_w_err.index[i])] = val_pos

    uncertain_df = _pd.DataFrame([act, inact, unk],
                                 index=['Active', 'Inactive', 'Unknown']).T

    # Calculate corrected values (assigning unknown into active/inactive)
    uncertain_df.loc[:, 'Active_corrected'] = _pd.Series(
        index=uncertain_df.index)
    uncertain_df.loc[:, 'Inactive_corrected'] = _pd.Series(
        index=uncertain_df.index)
    for i in uncertain_df.index:
        uncertain_df.loc[i, 'Active_corrected'], \
            uncertain_df.loc[i, 'Inactive_corrected'] = \
            _correct_unknown(
                uncertain_df['Active'].loc[i],
                uncertain_df['Inactive'].loc[i],
                uncertain_df['Unknown'].loc[i])
    uncertain_df = uncertain_df.T
    # Add total column
    uncertain_df.loc['Total_corrected'] = uncertain_df.loc[
                                              'Active_corrected'] + \
                                          uncertain_df.loc[
                                              'Inactive_corrected']

    # convert uncertainty dataframe back into separate columns for csv export
    cols = list(uncertain_df.columns)
    cols.insert(4, 'Percent')
    cols.insert(2, 'Length')
    cols.insert(0, 'Density')
    formatted_df = _pd.DataFrame(columns=cols, index=uncertain_df.index)
    for c in uncertain_df.columns:
        formatted_df[c] = uncertain_df[c].map(lambda x: x.std_dev)
        formatted_df[c.split('_')[0]] = uncertain_df[c].map(
            lambda x: x.nominal_value)

    return formatted_df
