API Summary
===========

Skeletons
+++++++++

..  autosummary::

    ~FIBbootstrap.skeleton.bootstrap_skel_stats
    ~FIBbootstrap.skeleton.find_nodes



Surfaces
++++++++

..  autosummary::

    ~FIBbootstrap.surface.process_surface_stats_lsm_ysz


Tortuosity
++++++++++

..  autosummary::

    ~FIBbootstrap.tortuosity.bootstrap_tort_stats


TPB
+++

..  autosummary::

    ~FIBbootstrap.tpb.bootstrap_tpb_stats
    ~FIBbootstrap.tpb.read_mv3d
    ~FIBbootstrap.tpb.write_mv3d
    ~FIBbootstrap.tpb.crop_tpb_data
    ~FIBbootstrap.tpb.get_bb_from_data
    ~FIBbootstrap.tpb.bb_volume
    ~FIBbootstrap.tpb.get_random_subvolume
    ~FIBbootstrap.tpb.path_length
    ~FIBbootstrap.tpb.network_length
    ~FIBbootstrap.tpb.split_paths_in_network
    ~FIBbootstrap.tpb.animate_cropped_data
    ~FIBbootstrap.tpb.get_bb_lines
    ~FIBbootstrap.tpb.get_box_and_corners


Utilities
+++++++++

..  autosummary::

    ~FIBbootstrap.utils.calculate_errors


Full Package API
================

Skeletons
+++++++++

..  automodule:: FIBbootstrap.skeleton
    :members:
    :private-members:
    :undoc-members:
    :show-inheritance:


Surfaces
++++++++

..  automodule:: FIBbootstrap.surface
    :members:
    :private-members:
    :undoc-members:
    :show-inheritance:

Tortuosity
++++++++++

..  automodule:: FIBbootstrap.tortuosity
    :members:
    :private-members:
    :undoc-members:
    :show-inheritance:


TPB
+++

..  automodule:: FIBbootstrap.tpb
    :members:
    :private-members:
    :undoc-members:
    :show-inheritance:

Utilities
+++++++++

..  automodule:: FIBbootstrap.utils
    :members:
    :private-members:
    :undoc-members:
    :show-inheritance: