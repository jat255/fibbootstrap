.. FIB Bootstrap documentation master file, created by
   sphinx-quickstart on Fri Apr  8 11:10:52 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

FIB Bootstrap Statistics
========================

..  toctree::
    :caption: Table of Contents
    :maxdepth: 3

    intro.rst
    install.rst
    avizo.rst
    tutorial.rst
    api_doc.rst

