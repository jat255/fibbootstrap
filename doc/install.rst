Installation
============

Requirements
++++++++++++

The following dependencies are needed to run the code (as-written).
Some details on installing them are given below:

..  csv-table::
    :header: Package, Source, PyPI
    :escape: \

    ``numpy``, `Source <Numpy_>`_, `PyPI <NumpyPYPI_>`_
    ``scipy``, `Source <scipy_>`_, `PyPI <scipyPYPI_>`_
    ``scikits.bootstrap``, `Source <skboot_>`_, `PyPI <skbootPYPI_>`_
    ``statsmodels``, `Source <statsmodels_>`_, `PyPI <statsmodelsPYPI_>`_
    ``tqdm``, `Source <tqdm_>`_, `PyPI <tqdmPYPI_>`_
    ``pandas``, `Source <pandas_>`_, `PyPI <pandasPYPI_>`_
    ``mayavi``, `Source <mayavi_>`_, `PyPI <mayaviPYPI_>`_
    ``vtk``, `Source <vtk_>`_


.. _Numpy: http://www.numpy.org/
.. _NumpyPYPI: https://pypi.python.org/pypi/numpy/1.11.0

.. _matplotlib: http://matplotlib.org/
.. _matplotlibPYPI: https://pypi.python.org/pypi/matplotlib/1.5.1

.. _scipy: https://www.scipy.org/scipylib/index.html
.. _scipyPYPI: https://pypi.python.org/pypi/scipy/0.7.0

.. _HyperSpy: http://www.hyperspy.org/
.. _HyperSpyPYPI: https://pypi.python.org/pypi/hyperspy/0.8.4

.. _seaborn: https://stanford.edu/~mwaskom/software/seaborn/
.. _seabornPYPI: https://pypi.python.org/pypi/seaborn

.. _tqdm: https://github.com/tqdm/tqdm/
.. _tqdmPYPI: https://pypi.python.org/pypi/tqdm

.. _PyQt4: https://www.riverbankcomputing.com/software/pyqt/download

.. _h5py: http://www.h5py.org/
.. _h5pyPYPI: https://pypi.python.org/pypi/h5py/2.5.0

.. _skimage: https://github.com/scikit-image/scikit-image
.. _skimagePYPI: https://pypi.python.org/pypi/scikit-image

.. _skboot: https://scikits.appspot.com/bootstrap
.. _skbootPyPI: https://pypi.python.org/pypi/scikits.bootstrap

.. _docopt: https://github.com/docopt/docopt
.. _docoptPYPI: https://pypi.python.org/pypi/docopt/0.6.2

.. _pandas: http://pandas.pydata.org/
.. _pandasPYPI: https://pypi.python.org/pypi/pandas/0.18.0

.. _statsmodels: http://statsmodels.sourceforge.net/
.. _statsmodelsPYPI: https://pypi.python.org/pypi/statsmodels

.. _mayavi: http://code.enthought.com/projects/mayavi/
.. _mayaviPYPI: https://pypi.python.org/pypi/mayavi

.. _vtk: http://www.vtk.org/
.. _vtkGohlke: http://www.lfd.uci.edu/~gohlke/pythonlibs/#vtk
.. _OpenCV: http://opencv.org/


A Note on VTK
+++++++++++++

VTK (the Visualization ToolKit) is a system that allows for easy visualization
of 3D data. It is only required if you wish to do visualization, and is a
dependency of the `Mayavi <mayavi_>`_ package.
Some of the code in the :py:mod:`~FIBbootstrap.tpb` module
uses this toolkit to enable a visualization of TPB networks and how
the code randomly subsamples the volume.

Windows
^^^^^^^

On Windows, the easiest way to get going is to use
`Cristoph Gohlke's <vtkGohlke_>`_ prebuilt binaries.
Download the correct binary for your Python version (2.7, 3.4, or 3.5)
and Python build (32 or 64 bit). Once downloaded, go to the folder where
the file is located, and install using ``pip``. For example:

..  code-block:: bash

    $ pip install VTK-7.0.0-cp35-cp35m-win_amd64.whl

Linux
^^^^^

On Linux, binary wheel distributions are not provided for packages such as
this. You can search for a VTK distribution in your package manager, but
chances are it is compiled with Python 2 bindings. This may be fine if you're
using Python 2.7, but you should really be using a Python 3.X version.
For Python 3, you will probably have to
download the source and compile the package using `CMake <https://cmake.org/>`_.

There are some helpful instructions `here <cmakeBuild_>`_. The important
``CMake`` flags to set to make sure you compile Python wrappers for 3.X are:
``VTK_WRAP_PYTHON``, ``VTK_PYTHON_VERSION``, and then all the ``PYTHON*``
flags. Make sure that the wrap Python flag is set, the Python version is set
to the version you have (`e.g.` ``3.5``), and then click `Configure`. CMake
should find your installed Python version and correctly fill all the ``PYTHON*``
flags correctly.

The other flag you want to make sure is set correctly is
``CMAKE_INSTALL_PREFIX``. This needs to be set so that the compiled
Python libraries get installed to a place on your Python ``PATH``.
On my system, CMake defaulted to setting this to ``/usr/local``, but I needed
to change it to ``/usr``, because my Python installation is in
``/usr/lib/python3.5``. Once all the flags are set, click `Configure` until
no more new flags appear, and then click `Generate`. This will generate
the ``Makefile`` for the installation.

Change to the build directory and then run the following code (replacing
the number `4` with however many cores you have on your machine):

..  code-block:: bash

    $ make -j4
    $ sudo make install

Check that Python can find the installation correctly by running:

..  code-block:: bash

    $ python -c "import vtk"

If this test does not report any errors, then your installation probably went
okay.

.. _cmakeBuild: http://www.vtk.org/Wiki/VTK/Configure_and_Build

Development Version Installation
++++++++++++++++++++++++++++++++

Currently, the only supported method of installation is to clone the
development version and install using ``pip``.
The latest version of the code should be available in the online
`repository <https://jat255@bitbucket.org/jat255/fibbootstrap.git>`_.
To get this version installed on your system: install the requirements from
above, clone the repository, and then install with ``pip``:

.. code-block:: bash

    $ git clone https://jat255@bitbucket.org/jat255/fibbootstrap.git
    $ cd fibbootstrap
    $ pip install -e ./

If you would like to use the Avizo scripts as well, those are available
as a `submodule` of this package, and can be obtained by doing a `recursive`
git clone:

.. code-block:: bash

    $ git clone --recursive https://jat255@bitbucket.org/jat255/fibbootstrap.git

