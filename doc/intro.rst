Introduction
============
This module is used to estimate errors on a number of FIB/SEM reconstruction
microstructural parameters, as performed in J. Taillon's 2016 paper_ (to be
published). For most of the parameters, the error is determined by subsampling
a random smaller volume of the reconstruction many times, calculating the
relevant parameters, and the using a `bootstrap`_ method to estimate
confidence intervals for the value.

These methods rely on a series of `Avizo`_ scripts that are documented in
the :ref:`avizoScripts` section, and available in a separate folder
in the `repository <https://bitbucket.org/jat255/avizo-scripts/>`_.


..  _paper: `Introduction`_
..  _bootstrap: https://en.wikipedia.org/wiki/Bootstrapping_(statistics)
..  _Avizo: http://www.fei.com/software/avizo3d/