#!/usr/bin/env bash
#  outputEdges.sh ".mv3d skeleton file"
#
#  searches through a skeleton file (as outputted by Amira in .mv3d format)
#  and reports the edges found
#  (this is useful to find original edge numbers after loops were removed)
#
#  set -xv

# input and output files
INPUT=$1
OUTPUT=edges.txt
TEMP1=`tempfile -d . -s ".txt"`
TEMP2=`tempfile -d . -s ".txt"`

display_usage() { 
    echo -e "\nUsage:\n$0 [.mv3d skeleton file] \n" 
} 

# if less than one arguments supplied, display usage 
if [  $# -le 0 ] 
    then 
    display_usage
    rm $TEMP1 $TEMP2
    exit 1
    fi 
 
# check whether user had supplied -h or --help . If yes display usage 
if [[ ( $# == "--help") ||  $# == "-h" ]]; then
    display_usage
    rm $TEMP1 $TEMP2
    exit 0
    fi 
 
# check to make sure skeleton file exists
if [ -f "$1" ]; then


# remove header information from skeleton file
sed '/^\#/d' "$1" > $TEMP1 

## remove blank lines
sed '/^$/d' $TEMP1 > $TEMP2
# this is a list of the edge numbers 

## find number of edges (number of unique lines in first 
## column of skeleton data)
awk '{print $1}' $TEMP2 | uniq > $OUTPUT

# clean up our mess
rm $TEMP1 $TEMP2


exit 0
fi

# couldn't find input file. clean up and quit.
display_usage
rm $TEMP1 $TEMP2
exit 1
