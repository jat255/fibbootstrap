#!/usr/bin/env bash
#  findNodes "loopfile" "nodefile" ".mv3d skeleton OUTPUT file"
#
#  searches through a loops file and removes each edge in that file
#  from the corresponding skeleton files. Resulting output is a .mv3d skeleton
#  that can be imported into Amira
#
#  set -xv

# input and output files
LOOP_INPUT=$1
SKEL_INPUT=$2
OUTPUT=${2%.*}.loopsRemoved.mv3d
#LOOPFILE=loops.txt
TEMP1=`mktemp -p . --suffix ".mv3d"`
#TEMP2=`tempfile -d . -s ".mv3d"`
TMP_OUTPUT=`mktemp -p . --suffix ".mv3d"`
LOOPEDGES=loopEdges.txt
LOOPPATTERN=loop_pattern

# check to make sure input files exist
if [ -f "$1" -a -f "$2" ]; then

# make sure that we are using the right output file
while :
do
  if [ -f "$OUTPUT" ]; then
      echo ""
      echo "Default output file \"$OUTPUT\" already exists. Delete (d) or rename (r)?"
      read x
      case $x in
       d)
        rm $OUTPUT
	echo "Deleted $OUTPUT"
	break
	;;
       r)
        echo "New output file name?"
	read OUTPUT
	;;
       *)
        echo "Invalid response \"$x\""
	;;
      esac
  else
      break
  fi
done


# remove header information from loop file
sed '/^\#/d' "$1" > $TEMP1 

# store identifying edge numbers in LOOPEDGES
cat $TEMP1 | awk '{print $2}' > $LOOPEDGES
noLoops=`wc -l $LOOPEDGES | awk '{print $1}'`

# format loop edges for input into grep
awk '{print "^"$1"\t"}' $LOOPEDGES > $LOOPPATTERN

# remove matching lines from the inputted skeleton
grep -v -f $LOOPPATTERN $SKEL_INPUT > $TMP_OUTPUT

# collapse double blank lines to single and add to OUTPUT
cat -s $TMP_OUTPUT > $OUTPUT

# clean up our mess
rm $TEMP1 $TMP_OUTPUT $LOOPPATTERN $LOOPEDGES

# rename loop/nodes files
echo "Renaming original loop and node files. (n) to cancel:"
read x
case $x in
       n)
	echo "Ok. Files left unchanged."
	break
	;;
       *)
        if [ -f "$LOOP_INPUT" ]; then
	    mv $LOOP_INPUT loops_beforeLoopRemoval.txt
	fi
	if [ ! -f nodes2.txt ]; then
	    echo "Default nodes file not found!"
	    while :
	    do 
		echo "What file should be renamed?"
		read y
		if [ -f "$y" ]; then
		    mv $y nodes_beforeLoopRemoval.txt
		    break
		else
		    echo "Could not find that file."
		    echo ""
		fi
	    done
	else
	    mv nodes2.txt nodes_beforeLoopRemoval.txt
	fi
	;;
      esac

exit 0
fi

# couldn't find input file. clean up and quit.
echo "Could not find input files. Check command line parameters."
echo "Parameters are: <loops file from findNodes.sh> <.mv3d skeleton file from Amira>"
echo ""
rm $TEMP1 $TMP_OUTPUT 
exit 1
